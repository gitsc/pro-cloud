/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.mybatis;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.cloud.common.data.util.IdUtils;

/**
 * 自定义id生成器
 * @author Aijm
 * @since 2020/3/14
 */
public class CustomIdGenerator implements IdentifierGenerator {


    @Override
    public Number nextId(Object entity) {
        return IdUtils.getNextId();
    }
}