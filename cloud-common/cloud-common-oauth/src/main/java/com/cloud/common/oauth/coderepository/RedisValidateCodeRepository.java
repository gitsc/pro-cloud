/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.oauth.coderepository;


import com.cloud.common.cache.util.RedisUtil;
import com.cloud.common.oauth.validate.ValidateCode;
import com.cloud.common.oauth.validate.ValidateCodeRepository;
import com.cloud.common.oauth.validate.ValidateCodeType;
import com.cloud.common.util.var.RedisKeys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;


/**
 * 基于redis的验证码存取器
 * @author Aijm
 * @since 2019/5/26
 */
@Component
@Slf4j
public class RedisValidateCodeRepository implements ValidateCodeRepository {


	/**
	 * 保存验证码
	 * @param request
	 * @param code
	 * @param type
	 */
	@Override
	public void save(ServletWebRequest request, ValidateCode code, ValidateCodeType type) {
		RedisUtil.putTime(RedisKeys.CODE_VALID, buildKey(request, type), code, 180L);
	}


	/**
	 * 获取验证码
	 * @param request
	 * @param type
	 * @return
	 */
	@Override
	public ValidateCode get(ServletWebRequest request, ValidateCodeType type) {
		Object value = RedisUtil.get(RedisKeys.CODE_VALID, buildKey(request, type));
		if (value == null) {
			return null;
		}
		return (ValidateCode) value;
	}

	/**
	 * Remove.
	 *
	 * @param request the request
	 * @param type    the type
	 */
	@Override
	public void remove(ServletWebRequest request, ValidateCodeType type) {
		RedisUtil.remove(RedisKeys.CODE_VALID, buildKey(request, type));
	}

	private String buildKey(ServletWebRequest request, ValidateCodeType type) {
		String deviceId = request.getHeader("deviceId");
		return type.toString().toLowerCase() + ":" + deviceId;
	}

}