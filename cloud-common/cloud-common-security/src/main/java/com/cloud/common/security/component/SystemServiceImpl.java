/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.security.component;

import com.cloud.common.data.user.SystemService;
import com.cloud.common.security.util.SecurityUtil;
import org.springframework.stereotype.Service;
/**
 * @Author Aijm
 * @Description  提供datam模块使用 获取到用户的id
 *      data 对每个对象进行数据库插入或者更新时的操作
 * @Date 2019/9/4
 */
@Service
public class SystemServiceImpl implements SystemService {


    /**
     * 获取到登录用户的id 为了封装
     *
     * @return
     */
    @Override
    public Long getUserId() {
        return SecurityUtil.getSecurityUserId();
    }

    /**
     * 获取到用户的租户id
     *
     * @return
     */
    @Override
    public Integer getUserTenantId() {
        return SecurityUtil.getSecurityTenantId();
    }

    /**
     * 获取到用户类型
     * @return
     */
    @Override
    public Integer getUserType() {
        return SecurityUtil.getSecurityUserType();
    }

    /**
     * 获取到用户名称
     *
     * @return
     */
    @Override
    public String getUserName() {
        return SecurityUtil.getSecurityUserName();
    }
}