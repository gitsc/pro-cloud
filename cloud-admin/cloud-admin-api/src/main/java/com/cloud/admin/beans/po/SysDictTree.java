/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.TreeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典项tree
 *
 * @author Aijm
 * @date 2019-09-05 20:00:25
 */
@Data
@TableName("sys_dict_tree")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "区域表")
public class SysDictTree extends TreeEntity<SysDictTree> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "字典类型code")
    private String typeCode;

    @ApiModelProperty(value = "编码；一般唯一")
    private String value;



}