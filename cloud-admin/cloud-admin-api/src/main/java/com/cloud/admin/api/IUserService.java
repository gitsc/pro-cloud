/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.api;

import com.cloud.admin.beans.dto.RoleDTO;
import com.cloud.admin.beans.po.SysMenu;


import java.util.List;

/**
 * 获取当前用户信息
 * @author Aijm
 * @since 2020/5/8
 */
public interface IUserService {

    /**
     * 获取用户的权限信息(菜单)
     * @param userId
     * @return
     */
    public List<SysMenu> getMenuList(Long userId);


    /**
     * 获取用户的角色信息
     * @param userId
     * @return
     */
    public List<RoleDTO> getRoleList(Long userId);
}