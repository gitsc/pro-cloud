/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.componse;

import com.cloud.common.security.component.IPermissionService;
import lombok.extern.slf4j.Slf4j;


/**
 * @author Aijm
 * @date 2019/6/25
 * 接口权限判断工具
 */
@Slf4j
public class PermissionServiceImpl implements IPermissionService {


	/**
	 * 判断接口是否有权限
	 * 		内部用户为最高权限
	 * @param permission
	 * @return
	 */
	@Override
	public boolean hasPermission(String permission) {
		log.debug("PermissionServiceImpl校验权限");
		return true;
	}

	/**
	 * 是否具有租户管理员
	 * 内部用户为最高权限
	 *
	 * @param permission
	 * @return
	 */
	@Override
	public boolean hasTenantPermissin(String permission) {
		return true;
	}

	/**
	 * 判断当前用户是不是具有某个角色
	 * 内部用户为最高权限
	 *
	 * @param enname
	 * @return
	 */
	@Override
	public boolean hasRole(String enname) {
		return true;
	}

	/**
	 * 判断是否登录
	 * @return
	 */
	@Override
	public boolean hasLogin() {
		return true;
	}


}