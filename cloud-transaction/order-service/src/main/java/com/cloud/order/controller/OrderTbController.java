/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.order.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.order.api.IAccountTbService;
import com.cloud.order.beans.po.AccountTb;
import com.cloud.order.beans.po.OrderTb;
import com.cloud.order.service.OrderTbService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

/**
 *
 *
 * @author Aijm
 * @date 2020-03-15 18:34:59
 */
@RestController
@RequestMapping("/ordertb" )
@Api(value = "ordertb", tags = "ordertb管理")
public class OrderTbController {

    @Autowired
    private OrderTbService orderTbService;

    @Autowired
    private IAccountTbService accountTbService;


    /**
     * 新增
     * @param orderTb
     * @return Result
     */
    @PostMapping
    public Result save(@RequestBody @Valid OrderTb orderTb) {

        orderTb.setCreateBy(1L);
        orderTb.setCommodityCode("1");
        orderTb.setUserId("1");
        orderTb.setCount(1);
        orderTb.setUpdateBy(1L);
        orderTbService.save(orderTb);

        AccountTb accountTb = new AccountTb();
        accountTb.setId(1L);
        accountTb.setMoney(100);
        accountTbService.updateById(accountTb);

        return Result.success("成功");
    }



}